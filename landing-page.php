<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/fontawesome.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.6.1/font/bootstrap-icons.css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

    <title>Landing Page 1</title>

    <style>
      @font-face { font-family: Montserrat; src: url('assets/fonts/montserrat/Montserrat-Light.otf'); } 
      /* @font-face { font-family: Montserrat; font-weight: bold; src: url('assets/fonts/montserrat/Montserrat-Bold.otf');} */

      html, body{
        /* font-size: calc(0.6em + 0.5vw); */
        font-family: Montserrat;
      }
      .icon-sm {
        font-size: 8px !important;
      }

      .icon-md {
        font-size: 16px !important;
      }

      .icon-xl {
        font-size: 32px !important;
      }

      .icon-xxl{
        font-size: 40px !important;
      }

      i.crop-top{
        line-height: 90% !important;
      }

      .navbar a{
        color: #343a40 !important;
      }

      .custom .fa-circle:before {
        border-radius: 50%;
        background: linear-gradient(red, blue);
        color: transparent;
      }

      i.fa-stack-1x{
        font-size: 0.5em !important;
      }
    </style>
  </head>
  <body>
    <header class="border-bottom">
      <div class="container">
          <div class="row">
            <div class="col-12 col-md-6 col-lg-6">
              <div class="d-flex justify-content-start">
                <i class="bi bi-bookmark-fill text-primary icon-xxl crop-top"></i>
                <p class="my-auto">Gratis Ebook 9 Cara Cerdas Menggunakan Domain [x]</p>
              </div>
            </div>
            <div class="col-12 col-md-6 col-lg-6">
              <div class="d-flex flex-xl-row justify-content-xl-end justify-content-between flex-row">
                <div class="p-3"><i class="bi bi-telephone-fill icon-md"></i> 0374-5305505</div>
                <div class="p-3"><i class="bi bi-chat-fill icon-md"></i> Live Chat</div>
                <div class="p-3"><i class="bi bi-person-circle icon-md"></i> Member Area</div>
              </div>
            </div>
          </div>
      </div>
    </header>

    <div class="border-bottom">
      <div class="container-sm px-0">
        <nav class="navbar navbar-expand-lg navbar-light p-0">
          <a class="navbar-brand" href="#">
            <img src="assets/images/logo-niagahoster.png" alt="" class="img-fluid">
          </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item">
                <a class="nav-link" href="#">Hosting</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Domain</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Server</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Website</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Afiliasi</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Promo</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Pembayaran</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Review</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Kontak</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Blog</a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    </div>

    <div class="container-fluid">
      <div class="container my-5">
        <div class="row">
          <div class="col-sm-12 col-md-6 col-lg-6">
            <h2 class="text-bold">PHP Hosting</h2>
            <h5>Cepat, handal, penuh dengan PHP yang Anda butuhkan</h5>
            <ul class="list-group my-3">
              <li class="list-group-item border-0 p-0">
                <i class="bi bi-check-circle-fill text-success"></i>
                Solusi PHP untuk performa query yang lebih cepat.
              </li>
              <li class="list-group-item border-0 p-0">
                <i class="bi bi-check-circle-fill text-success"></i> 
                Konsumsi memori yang lebih rendah.
              </li>
              <li class="list-group-item border-0 p-0">
                <i class="bi bi-check-circle-fill text-success"></i> 
                Support PHP 5.3. PHP 5.4. PHP 5.5. PHP5.6. PHP7.
              </li>
              <li class="list-group-item border-0 p-0">
                <i class="bi bi-check-circle-fill text-success"></i> 
                Fitur enkripsi IonCube dan Zend Guard Loaders
              </li>
            </ul>
          </div>
          <div class="col-sm-12 col-md-6 col-lg-6">
            <img src="assets/svg/illustration banner PHP hosting-01.svg" class="img-fluid">
          </div>
        </div>
      </div>

      <div class="container text-center">
        <div class="row d-flex justify-content-center align-items-center">
          <div class="col-sm-3 col-md-3 col-lg-3">
            <embed src="assets/svg/illustration-banner-PHP-zenguard01.svg" class="img-fluid">
          </div>
          <div class="col-sm-3 col-md-3 col-lg-3">
            <embed src="assets/svg/icon-composer.svg" class="img-fluid">
          </div>
          <div class="col-sm-3 col-md-3 col-lg-3">
            <embed src="assets/svg/icon-php-hosting-ioncube.svg" class="img-fluid">
          </div>
        </div>
        <div class="row d-flex justify-content-center">
          <div class="col-sm-3 col-md-3 col-lg-3">
            <p class="align-bottom">PHP Zend Guard Loader</p>
          </div>
          <div class="col-sm-3 col-md-3 col-lg-3">
            <div>PHP Composer</div>
          </div>
          <div class="col-sm-3 col-md-3 col-lg-3">
            <div>PHP IonCube Loader</div>
          </div>
        </div>
      </div>

      <div class="container text-center py-5">
          <h3>Paket Hosting Singapura yang Tepat</h1>
          <h4>Diskon 40% + Domain dan SSL Gratis untuk Anda</h3>
      </div>

      <div class="container py-5">
        <div class="row">
          <div class="col-sm-12 col-md-3 col-lg-3 p-0">
            <div class="border">
              <ul class="list-group list-group-flush text-center">
                <li class="list-group-item">
                  <p class="h3 font-weight-bold">Bayi</p>
                </li>
                <li class="list-group-item">
                  <p>Rp 19.900</p>
                  <p>
                    Rp 
                    <span class="h1">14</span>
                    <span class="font-weight-bold">.900/bln</span>
                  </p>
                </li>
                <li class="list-group-item">
                  <p>938 Pengguna Terdaftar</p>
                </li>
                <li class="list-group-item">
                  <p>0.5X RESOURCE POWER</p>
                  <p>500 MB Disk Space</p>
                  <p>Unlimited Bandwith</p>
                  <p>Unlimited Databases</p>
                  <p>1 Domain</p>
                  <p>Instant Backup</p>
                  <p>Unlimited SSL Gratis Selamanya</p>

                  <a class="btn btn-outline-secondary rounded-pill" href="#" role="button">Pilih Sekarang</a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-sm-12 col-md-3 col-lg-3 p-0">
            <div class="border">
              <ul class="list-group list-group-flush text-center">
                <li class="list-group-item">
                  <p class="h3 font-weight-bold">Pelajar</p>
                </li>
                <li class="list-group-item">
                  <p>Rp 46.900</p>
                  <p>
                    Rp 
                    <span class="h1">23</span>
                    <span class="font-weight-bold">.450/bln</span>
                  </p>
                </li>
                <li class="list-group-item">
                  <p>4.168 Pengguna Terdaftar</p>
                </li>
                <li class="list-group-item">
                  <p>1X RESOURCE POWER</p>
                  <p>Unlimited Disk Space</p>
                  <p>Unlimited Bandwith</p>
                  <p>Unlimited POP3 Email</p>
                  <p>Unlimited Databases</p>
                  <p>10 Addon Domains</p>
                  <p>Instant Backup</p>
                  <p>Domain Gratis Selamanya</p>
                  <p>Unlimited SSL Gratis Selamanya</p>

                  <a class="btn btn-outline-secondary rounded-pill" href="#" role="button">Pilih Sekarang</a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-sm-12 col-md-3 col-lg-3 p-0">
            <div class="border border-primary">
              <ul class="list-group list-group-flush text-center">
                <li class="list-group-item bg-primary text-white d-flex justify-content-start">
                  <img src="assets/images/icon-best-seller.png" width="35%" class="image-fluid mt-n3 ml-n4">
                  <p class="h3 font-weight-bold">Personal</p>
                </li>
                <li class="list-group-item bg-primary text-white">
                  <p>Rp 58.900</p>
                  <p>
                    Rp 
                    <span class="h1">38</span>
                    <span class="font-weight-bold">.900/bln</span>
                  </p>
                </li>
                <li class="list-group-item bg-primary text-white">
                  <p>10.017 Pengguna Terdaftar</p>
                </li>
                <li class="list-group-item">
                  <p>2X RESOURCE POWER</p>
                  <p>Unlimited Disk Space</p>
                  <p>Unlimited Bandwith</p>
                  <p>Unlimited POP3 Email</p>
                  <p>Unlimited Databases</p>
                  <p>Unlimited Addon Domains</p>
                  <p>Instant Backup</p>
                  <p>Domain Gratis Selamanya</p>
                  <p>Unlimited SSL Gratis Selamanya</p>
                  <p>Private Name Server</p>
                  <p>SpamAssasin Mail Protection</p>

                  <a class="btn btn-outline-primary rounded-pill" href="#" role="button">Pilih Sekarang</a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-sm-12 col-md-3 col-lg-3 p-0">
            <div class="border">
              <ul class="list-group list-group-flush text-center">
                <li class="list-group-item">
                  <p class="h3 font-weight-bold">Bisnis</p>
                </li>
                <li class="list-group-item">
                  <p>Rp 109.900</p>
                  <p>
                    Rp 
                    <span class="h1">65</span>
                    <span class="font-weight-bold">.900/bln</span>
                  </p>
                </li>
                <li class="list-group-item">
                  <p>3.552 Pengguna Terdaftar</p>
                </li>
                <li class="list-group-item">
                  <p>3X RESOURCE POWER</p>
                  <p>Unlimited Disk Space</p>
                  <p>Unlimited Bandwith</p>
                  <p>Unlimited POP3 Email</p>
                  <p>Unlimited Databases</p>
                  <p>Unlimited Addon Domains</p>
                  <p>Magic Auto Backup & Restore</p>
                  <p>Domain Gratis Selamanya</p>
                  <p>Unlimited SSL Gratis Selamanya</p>
                  <p>Private Name Server</p>
                  <p>Prioritas Layanan Support</p>
                  <p class="text-primary">
                    <i class="bi bi-star-fill"></i>
                    <i class="bi bi-star-fill"></i>
                    <i class="bi bi-star-fill"></i>
                    <i class="bi bi-star-fill"></i>
                    <i class="bi bi-star-fill"></i>
                  </p>
                  <p>SpamExpert Pro Mail Protection</p>

                  <a class="btn btn-outline-primary rounded-pill" href="#" role="button">Pilih Sekarang</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      
      <div class="container">
        <div class="text-center h3">Powerful dengan Limit PHP yang Lebih Besar</div>
        <div class="row py-5">
          <div class="col-sm-12 col-md-6 col-lg-6 pl-5 pr-3 py-lg-0 py-2">
            <ul class="list-group">
              <li class="list-group-item d-flex justify-content-center align-items-center">
                <i class="bi bi-check-circle-fill text-success"></i>
                <span class="mx-auto">max execution time 300s</span>
              </li>
              <li class="list-group-item d-flex justify-content-center align-items-center">
                <i class="bi bi-check-circle-fill text-success"></i>
                <span class="mx-auto">max execution time 300s</span>
              </li>
              <li class="list-group-item d-flex justify-content-center align-items-center">
                <i class="bi bi-check-circle-fill text-success"></i>
                <span class="mx-auto">max execution time 300s</span>
              </li>
              <li class="list-group-item d-flex justify-content-center align-items-center">
                <i class="bi bi-check-circle-fill text-success"></i>
                <span class="mx-auto">max execution time 300s</span>
              </li>
            </ul>
          </div>
          <div class="col-sm-12 col-md-6 col-lg-6 pl-5 pr-3 py-lg-0 py-2">
            <ul class="list-group">
              <li class="list-group-item d-flex justify-content-center align-items-center">
                <i class="bi bi-check-circle-fill text-success"></i>
                <span class="mx-auto">max execution time 300s</span>
              </li>
              <li class="list-group-item d-flex justify-content-center align-items-center">
                <i class="bi bi-check-circle-fill text-success"></i>
                <span class="mx-auto">max execution time 300s</span>
              </li>
              <li class="list-group-item d-flex justify-content-center align-items-center">
                <i class="bi bi-check-circle-fill text-success"></i>
                <span class="mx-auto">max execution time 300s</span>
              </li>
              <li class="list-group-item d-flex justify-content-center align-items-center">
                <i class="bi bi-check-circle-fill text-success"></i>
                <span class="mx-auto">max execution time 300s</span>
              </li>
            </ul>
          </div>
        </div>
      </div>

      <div class="container">
        <div class="h3 text-center"> Semua Paket Hosting Sudah termasuk</div>
        <div class="row">
          <div class="col-sm-6 col-md-4 col-lg-4 text-center p-3">
            <embed src="assets/svg/icon PHP Hosting_PHP Semua Versi.svg" width="15%" class="img-fluid">
            <p class="h5 font-weight-bold m-0">PHP Semua Versi</p>
            <p class="m-0">Pilih mulai dari versi PHP 5.3 s/d php 7. Ubah sesuka Anda</p>
          </div>
          <div class="col-sm-6 col-md-4 col-lg-4 text-center p-3">
            <embed src="assets/svg/icon PHP Hosting_My SQL.svg" width="15%" class="img-fluid">
            <p class="h5 font-weight-bold m-0">MySQL Versi 5.6</p>
            <p class="m-0">Nikmati MySql versi terbaru, tercepat dan kaya akan fitur.</p>
          </div>
          <div class="col-sm-6 col-md-4 col-lg-4 text-center p-3">
            <embed src="assets/svg/icon PHP Hosting_CPanel.svg" width="15%" class="img-fluid">
            <p class="h5 font-weight-bold m-0">Panel Hosting cPanel</p>
            <p class="m-0">Kelola website dengan panel canggih yang familiar di hati anda.</p>
          </div>
          <div class="col-sm-6 col-md-4 col-lg-4 text-center p-3">
            <embed src="assets/svg/icon PHP Hosting_garansi uptime.svg" width="15%" class="img-fluid">
            <p class="h5 font-weight-bold m-0">garansi Uptime 99.9%</p>
            <p class="m-0">Data center yang mendukung kelangsungan website Anda 24/7.</p>
          </div>
          <div class="col-sm-6 col-md-4 col-lg-4 text-center p-3">
            <embed src="assets/svg/icon PHP Hosting_InnoDB.svg" width="15%" class="img-fluid">
            <p class="h5 font-weight-bold m-0">Database InnoDB Unlimited</p>
            <p class="m-0">Jumlah data dan ukuran database yang tumbuh sesuai kebutuhan anda.</p>
          </div>
          <div class="col-sm-6 col-md-4 col-lg-4 text-center p-3">
            <embed src="assets/svg/icon PHP Hosting_My SQL remote.svg" width="15%" class="img-fluid">
            <p class="h5 font-weight-bold m-0">Wildcard Remote MySQL</p>
            <p class="m-0">Mendukung s/d 25 max_user_connections dan 100 max_connections.</p>
          </div>
        </div>
      </div>

      <div class="container">
        <div class="h3 text-center"> Mendukung Penuh Framework Laravel</div>
        <div class="row">
          <div class="col-sm-12 col-md-6 col-lg-6 p-3">
            <div>
              Tak perlu menggunakan dedicated server ataupun vps yang mahal. Layanan PHP hosting murah kami mendukung penuh framework favorit Anda.
            </div>
            <div>
              <ul class="list-group my-3">
                <li class="list-group-item border-0 p-0">
                  <i class="bi bi-check-circle-fill text-success"></i>
                  Solusi PHP untuk performa query yang lebih cepat.
                </li>
                <li class="list-group-item border-0 p-0">
                  <i class="bi bi-check-circle-fill text-success"></i> 
                  Konsumsi memori yang lebih rendah.
                </li>
                <li class="list-group-item border-0 p-0">
                  <i class="bi bi-check-circle-fill text-success"></i> 
                  Support PHP 5.3. PHP 5.4. PHP 5.5. PHP5.6. PHP7.
                </li>
                <li class="list-group-item border-0 p-0">
                  <i class="bi bi-check-circle-fill text-success"></i> 
                  Fitur enkripsi IonCube dan Zend Guard Loaders
                </li>
              </ul>
            </div>
            <small>NB : Composer dan SSH hanya tersedia pada paket Personal dan Bisnis.</small>
            <button class="btn btn-primary rounded-pill mt-3">Pilih Hosting Anda</button>
          </div>
          <div class="col-sm-12 col-md-6 col-lg-6 p-3">
            <embed src="assets/svg/illustration banner support laravel hosting.svg" class="img-fluid">
          </div>
        </div>
      </div>

      <div class="container">
        <div class="h3 text-center">Modul Lengkap untuk menjalankan Aplikasi PHP Anda.</div>
        <div class="row p-3">
          <div class="col-6 col-md-6 col-lg-3">
            <div class="d-flex justify-content-center">
              <div class="pl-5">
                <p class="m-0">IcePHP</p>
                <p class="m-0">apc</p>
                <p class="m-0">apcu</p>
                <p class="m-0">apm</p>
                <p class="m-0">ares</p>
                <p class="m-0">bcmath</p>
                <p class="m-0">bcompiler</p>
                <p class="m-0">big_init</p>
                <p class="m-0">bitset</p>
                <p class="m-0">bloomy</p>
                <p class="m-0">bz2_filter</p>
                <p class="m-0">clamav</p>
                <p class="m-0">coin_acceptor</p>
                <p class="m-0">crack</p>
                <p class="m-0">dba</p>
              </div>
            </div>
          </div>
          <div class="col-6 col-md-6 col-lg-3">
            <div class="d-flex justify-content-center">
              <div class="pl-5">
                <p class="m-0">http</p>
                <p class="m-0">huffman</p>
                <p class="m-0">idn</p>
                <p class="m-0">igbinary</p>
                <p class="m-0">imagick</p>
                <p class="m-0">imap</p>
                <p class="m-0">include</p>
                <p class="m-0">inotify</p>
                <p class="m-0">interbase</p>
                <p class="m-0">intl</p>
                <p class="m-0">ioncube_loader</p>
                <p class="m-0">ioncube_loader_4</p>
                <p class="m-0">jsmin</p>
                <p class="m-0">json</p>
                <p class="m-0">idap</p>
              </div>
            </div>
          </div>
          <div class="col-6 col-md-6 col-lg-3">
            <div class="d-flex justify-content-center">
              <div class="pl-5">
                <p class="m-0">nd_pdo_mysql</p>
                <p class="m-0">oauth</p>
                <p class="m-0">oci8</p>
                <p class="m-0">odbc</p>
                <p class="m-0">opache</p>
                <p class="m-0">pdf</p>
                <p class="m-0">pdo</p>
                <p class="m-0">pdo_dblib</p>
                <p class="m-0">pdo_firebird</p>
                <p class="m-0">pdo_mysql</p>
                <p class="m-0">pdo_odbc</p>
                <p class="m-0">pdo_pgsql</p>
                <p class="m-0">pdo_sqlite</p>
                <p class="m-0">pgsql</p>
                <p class="m-0">phalcon</p>
              </div>
            </div>
          </div>
          <div class="col-6 col-md-6 col-lg-3">
            <div class="d-flex justify-content-center">
              <div class="pl-5">
                <p class="m-0">stats</p>
                <p class="m-0">stem</p>
                <p class="m-0">stomp</p>
                <p class="m-0">suhosin</p>
                <p class="m-0">sybase_ct</p>
                <p class="m-0">sysvmsg</p>
                <p class="m-0">sysvsem</p>
                <p class="m-0">sysvshm</p>
                <p class="m-0">tidy</p>
                <p class="m-0">timezonedb</p>
                <p class="m-0">trader</p>
                <p class="m-0">transit</p>
                <p class="m-0">upload_progress</p>
                <p class="m-0">uri_template</p>
                <p class="m-0">uuid</p>
              </div>
            </div>
          </div>
        </div>
        <div class="row justify-content-center my-3">
          <div class="btn btn-outline-dark rounded-pill text-center">Selengkapnya</div>
        </div>
      </div>

      <div class="container">
        <div class="row">
          <div class="col-12 col-md-6 col-lg-6">
            <p class="h3">Linux Hosting Yang Stabil dengan Teknologi LVE</p>
            <p> Supermicro intel Xeon 24-cores server dengan ram 128 GB dan teknologi cloudlinux untuk stabilitas server anda. dilengkapi dengan ssd untuk kecepatan MYSQL dan caching. Apache load balancer berbasis litespeed technologies, CageFS security. Raid-10 protection dan auto backup untuk keamanan website PHP anda.</p>
            <a href="#" class="btn btn-primary rounded-pill">Pilih Hosting Anda</a>
          </div>
          <div class="col-12 col-md-6 col-lg-6">
            <img src="assets/images/Image support.png" class="image-fluid">
          </div>
        </div>
      </div>

    </div>

    <!-- <div class="container-fluid mt-5 bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-12 col-md-6 col-lg-6">
            <p>Bagikan Jika Anda Menyukai halaman ini.</p>
          </div>
          <div class="col-12 col-md-6 col-lg-6">
            <div class="d-flex justify-content-start">
              <i class="fab fa-facebook-square fa-2x"></i>
              <span class="p-1 border">80k</span>
              <i class="fab fa-twitter-square fa-2x"></i>
              <span class="p-1 border">450</span>
              <i class="fab fa-google-plus-square fa-2x"></i>
              <span class="p-1 border">1900</span>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid bg-primary">
      <div class="container d-flex justify-content-between py-5">
        <div class="h3 text-white">Perlu BANTUAN? Hubungi Kami : 0274-5305505</div>
        <div class="btn btn-outline-light rounded-pill pt-2 px-4">
          <i class="fas fa-user pr-3"></i>
          Live Chat
        </div>
      </div>
    </div> -->

    <!-- <footer class="container-fluid bg-dark">
      <div class="container py-5 text-light">
        <div class="row">
          <div class="col-3">
            <div class="h6">HUBUNGI Kami</div>

            <div class="pt-3 pb-4">
              <p class="m-0">0274-5305505</p>
              <p class="m-0">Senin - Minggu</p>
              <p class="m-0">24 Jam Nonstop</p>
            </div>

            <div class="pt-3 pb-4">
              <p class="m-0">Jl.Selokan Mataram Monjali</p>
              <p class="m-0">Karangjati MT I/304</p>
              <p class="m-0">Sinduadi, Mlati, Sleman</p>
              <p class="m-0">Yogyakarta 55284</p>
            </div>
          </div>

          <div class="col-3">
            <div class="h6">LAYANAN</div>

            <div class="pt-3 pb-4">
              <p class="m-0">Domain</p>
              <p class="m-0">Shared Hosting</p>
              <p class="m-0">Cloud VPS Hosting</p>
              <p class="m-0">Web Builder</p>
              <p class="m-0">keamanan SSL / HTTPS</p>
              <p class="m-0">Jasa Pembuatan Website</p>
              <p class="m-0">Program Affiliasi</p>
            </div>
          </div>

          <div class="col-3">
            <div class="h6">Service Hosting</div>

            <div class="pt-3 pb-4">
              <p class="m-0">Hosting Murah</p>
              <p class="m-0">Hosting Indonesia</p>
              <p class="m-0">Hosting Singapore SG</p>
              <p class="m-0">Hosting PHP</p>
              <p class="m-0">Hosting Wordpress</p>
              <p class="m-0">Hosting Laravel</p>
            </div>
          </div>

          <div class="col-3">
            <div class="h6">TUTORIAL</div>

            <div class="pt-3 pb-4">
              <p class="m-0">Knoeledgebase</p>
              <p class="m-0">Blog</p>
              <p class="m-0">Cara Pembayaran</p>
            </div>
          </div>

          <div class="col-3">
            <div class="h6">TENTANG KAMI</div>

            <div class="pt-3 pb-4">
              <p class="m-0">Tim Niagahoster</p>
              <p class="m-0">Karir</p>
              <p class="m-0">Events</p>
              <p class="m-0">Penawaran & Promo Spesial</p>
              <p class="m-0">KOntak Kami</p>
            </div>
          </div>

          <div class="col-3">
            <div class="h6">KENAPA PILIH NIAGAHOSTER</div>

            <div class="pt-3 pb-4">
              <p class="m-0">Support Terbaik</p>
              <p class="m-0">Garansi Harga Termurah</p>
              <p class="m-0">Domain Gratis Selamanya</p>
              <p class="m-0">Datacenter Hosting Terbaik</p>
              <p class="m-0">Review Pelanggan</p>
            </div>
          </div>

          <div class="col-3">
            <div class="h6">NEWSLETTER</div>

            <div class="pt-3 pb-4">
              <div class="row">
                <div class="input-group bg-white rounded-pill p-2">
                  <input type="text" class="form-control rounded-pill border-0" id="" placeholder="Email" aria-describedby="inputGroupPrepend2" required>
                  <div class="input-group-prepend">
                    <button type="submit" class="btn btn-primary btn-sm rounded-pill" id="inputGroupPrepend2">Berlangganan</button>
                  </div>
                </div>
              </div>

              <small>Dapatkan promo dan konten menarik dari oenyedia hosting terbaik Anda.</small>
            </div>
          </div>

          <div class="col-3">
            <div class="pt-3 pb-4">
              <div class="row py-4">
                <div class="col-4">
                  <span class="fa-stack fa-2x mx-auto">
                    <i class="far fa-circle fa-stack-2x"></i>
                    <i class="fab fa-facebook-f fa-stack-1x"></i>
                  </span>
                </div>
                <div class="col-4">
                  <span class="fa-stack fa-2x">
                    <i class="far fa-circle fa-stack-2x"></i>
                    <i class="fab fa-twitter fa-stack-1x"></i>
                  </span>
                </div>
                <div class="col-4">
                  <span class="fa-stack fa-2x">
                    <i class="far fa-circle fa-stack-2x"></i>
                    <i class="fab fa-google-plus-g fa-stack-1x"></i>
                  </span>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>

      <div class="container border-bottom py-3 text-light">
        <div class="text-uppercase">Pembayaran</div>
        <div class="row d-flex justify-content-start p-3">
          <div class="col-1 border border-light rounded p-1 mr-2">
            <embed src="assets/images/pembayaran/bca.svg" width="100%" height="30">
          </div>
          <div class="col-1 border border-light rounded p-1 mr-2">
            <embed src="assets/images/pembayaran/mandiri.svg" width="100%" height="30">
          </div>
          <div class="col-1 border border-light rounded p-1 mr-2">
            <embed src="assets/images/pembayaran/bni.svg" width="100%" height="30">
          </div>
          <div class="col-1 border border-light rounded p-1 mr-2">
            <embed src="assets/images/pembayaran/visa.svg" width="100%" height="30">
          </div>
          <div class="col-1 border border-light rounded p-1 mr-2">
            <embed src="assets/images/pembayaran/mastercard.svg" width="100%" height="30">
          </div>
          <div class="col-1 border border-light rounded p-1 mr-2">
            <embed src="assets/images/pembayaran/atm-bersama.png" width="100%" height="30">
          </div>
          <div class="col-1 border border-light rounded p-1 mr-2">
            <embed src="assets/images/pembayaran/permata.svg" width="100%" height="30">
          </div>
          <div class="col-1 border border-light rounded p-1 mr-2">
            <embed src="assets/images/pembayaran/prima.png" width="100%" height="30">
          </div>
          <div class="col-1 border border-light rounded p-1 mr-2">
            <embed src="assets/images/pembayaran/alto.png" width="100%" height="30">
          </div>
        </div>
        <div class="mb-3">Aktifitas instant dengan e-payment Hosting dan domain langsung aktif.</div>
      </div>

      <div class="container">
        <div class="row text-light">
          <div class="col-9">
            <p>Copyright &copy2016 Niagahoster | Hosting powered by PHP7. CloudLinux, CloudFlare, BitNinja, and DC Biznet Technovillage Jakarta</p>
            <p>Cloud VPS Murah powered by Webuzo Softaculous, Intel SSD and cloud computing technology</p>
          </div>
          <div class="col-3">
            Syarat dan Ketentuan | Kebijakan Privasi
          </div>
        </div>
      </div>
    </footer> -->

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
  </body>
</html>